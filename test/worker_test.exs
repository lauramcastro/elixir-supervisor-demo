defmodule WorkerTest do
  use ExUnit.Case
  import ExUnit.CaptureLog
  require Logger

  setup do
    {:ok, pid} = SupDemo.Worker.start_link(9)
    {:ok, pid: pid}
  end

  test "worker changes state when sent :increment message", context do
    pid = context[:pid]

    captured = capture_log(fn ->
      send pid, :increment
    end)

    assert captured =~ "[info] Elixir.SupDemo.Worker_9: increments 9"
  end

  test "worker dies normally after nine :increment messages", context do
    pid = context[:pid]

    send pid, :increment
    send pid, :increment
    send pid, :increment
    send pid, :increment
    send pid, :increment
    send pid, :increment
    send pid, :increment
    send pid, :increment
    send pid, :increment
    Process.sleep(10)

    refute Process.alive?(pid)
  end

 end
