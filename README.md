# Elixir Supervisors Demo

This is a demo project to illustrate the capabilities and behaviour of the
different process supervision strategies in Elixir.

## How to run

You can run this demo either as a no-halt process:

```
$ mix run --no-halt
```

but to actually be able to kill and thus observe the supervision strategies
in action you will need to launch it more interactively from a shell, so that you
can concurrently use the `:observer` to see what's going on, and randomly kill
worker processes:

```
$ iex -S mix 
```

![Observer displaying this demo app supervision tree](doc/observer.png)

The demo illustrates the strategy specified by the `@demo_mode` module variable
on line 9 of module `SupDemo.Supervisor`. To showcase other
supervision strategies, simply change the value of the variable, recompile and
relaunch. Of course, feel also free to change how many workers the supervisor
spawns at the beginning (currently lines 24-27 of the same module), and also how
each of the spawned children behaves in terms of restarting.
