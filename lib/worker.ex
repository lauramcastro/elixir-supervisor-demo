defmodule SupDemo.Worker do
  @moduledoc """
  This is a sample worker process.
  It "counts" incrementally from a given number.
  """
  use GenServer
  require Logger

  @doc """
  Start function for worker processes, receives
  an integer that will be used as basis for its dummy work.
  """
  @spec start_link(integer()) :: :ignore | {:error, any()} | {:ok, pid()}
  def start_link(start) when is_integer(start) do
    GenServer.start_link(__MODULE__, start, name: String.to_atom("#{__MODULE__}_#{start}"))
  end

  ## Callbacks
  @impl true
  def init(start) do
    Logger.info("Worker initialized")
    worker_name = String.to_atom("#{__MODULE__}_#{start}")
    {:ok, _pid} = Task.start(fn -> increment(worker_name) end)
    {:ok, {worker_name, start}}
  end

  @impl true
  def handle_info(:increment, _state = {worker_name, current}) do
    Logger.info("#{worker_name}: increments #{current}")
    {:ok, _pid} = Task.start(fn -> increment(worker_name) end)
    next = "#{current}#{current}"
    if String.length(next)< 60 do
      {:noreply, {worker_name, String.to_integer("#{current}#{current}")}}
    else
      {:stop, :normal, current}
    end
  end

  defp increment(worker) do
    Enum.random(5_000..10_000) |> Process.sleep()
    send(worker, :increment)
  end
end
