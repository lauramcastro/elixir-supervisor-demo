defmodule SupDemo.Application do
  @moduledoc """
  This is the main application module, where
  the process tree of the system is created.

  At the very least, there will be _one_ supervisor
  at root level.
  """

  use Application

  @impl true
  @spec start(atom(), any()) :: {:ok, pid()} | {:error, any()}
  def start(_type, _args) do
    children = [
      # Starts a worker by calling: SupDemo.Worker.start_link(arg)
      {SupDemo.Supervisor, []}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: SupDemo.AppSupervisor]
    Supervisor.start_link(children, opts)
  end
end
