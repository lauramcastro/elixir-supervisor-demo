defmodule SupDemo.Supervisor do
  @moduledoc """
  This is a module-based supervisor.
  """

  use Supervisor

  # change this using :one_for_one | :one_for_all | :rest_for_one to experiment
  @demo_mode :one_for_all

  @doc """
  Main function to start the supervisor.
  """
  @spec start_link(atom()) :: {:ok, pid()}
  def start_link(_arg) do
    Supervisor.start_link(__MODULE__, @demo_mode, name: __MODULE__)
  end

  @impl true
  def init(demo_mode), do: Supervisor.init(children(), strategy: demo_mode)

  defp children(),
    do: [
      Supervisor.child_spec({SupDemo.Worker, 1}, id: {Worker, 1}, restart: :permanent), # always restarted
      Supervisor.child_spec({SupDemo.Worker, 2}, id: {Worker, 2}, restart: :transient), # restarted unless :normal or :shutdown
      Supervisor.child_spec({SupDemo.Worker, 3}, id: {Worker, 3}, restart: :temporary), # never restarted
      Supervisor.child_spec({SupDemo.Worker, 4}, id: {Worker, 4}) # default: :permanent
    ]
end
